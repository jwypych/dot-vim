" Vim indent plugin OVERRIDE
" Language: Yaml

" Stop auto-indent behavior on specific keys
"   When in `indentexpr` mode (to check if it's active: `:set indentexpr?`),
"   certain keys will attempt to set the correct amount of indentation on
"   the current line. These keys can be shown with `:set indentkeys?`.
"     - see `:h indentkeys-format`
"     - remove # from list to avoid re-indenting comments
set indentkeys-=0#
