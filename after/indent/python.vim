" Vim indent plugin OVERRIDE
" Language: Python

" Stop auto-indent behavior on specific keys
"   When in `indentexpr` mode (to check if it's active: `:set indentexpr?`),
"   certain keys will attempt to set the correct amount of indentation on
"   the current line. These keys can be shown with `:set indentkeys?`.
"     - note that a bare : means indenting will happen when a : is typed
"       after a label or case statement. <:> means ANY :
"     - see `:h indentkeys-format`
set indentkeys-=<:>
