" Vim filetype plugin OVERRIDE
" Language: Markdown

" Conceal italic, bold, marks. Concealed characters become visible
" when cursors navigates to the line.
setlocal conceallevel=2
