" if file being edited is in /var/log and is named everything.log, errors.log, etc., set
" filetype to "messages", which triggers log syntax coloring
if expand("%:p:h") ==# "/var/log" && "everything.log errors.log" =~ expand("%:t") || "log.cat" =~ expand("%:t")
    setlocal filetype=messages
endif

