" Configure runtimepath and packpath for vim (since it's not xdg-compliant)
if !has('nvim')
  " Note: for this to work, you need `ln -s ~/.config/nvim/init.vim ~/.vimrc
  set runtimepath^=~/.config/nvim,~/.local/share/nvim/site runtimepath+=~/.config/nvim/after
  let &packpath = &runtimepath
endif

" Check for vim-plug and install if not already installed
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.local/share/nvim/site'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugins
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.local/share/nvim/plugged')
" List of plugins
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-fugitive'
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
Plug 'ojroques/vim-oscyank'
Plug 'easymotion/vim-easymotion'
Plug 'whiteinge/diffconflicts'
call plug#end()
" Plugins are visible to Vim after above call


" Mappings (remember comments have to be on separate lines)
let mapleader=" "
inoremap jk <Esc>
" Insert hard tab (since we use 4 spaces)
inoremap <S-Tab> <C-V><Tab>
" Move between windows
nnoremap <C-j> <C-W><C-j>
nnoremap <C-k> <C-W><C-k>
nnoremap <C-l> <C-W><C-l>
nnoremap <C-h> <C-W><C-h>
" Toggle between nothing and hybrid/relative line numbers (`nu rnu`)
nnoremap <leader>n :set nu! rnu!<CR>
" Toggle between standard and hybrid/relative line numbers
nnoremap <leader>h :set nu rnu!<CR>
" Copy visual-mode selected text to OS clipboard
vnoremap <leader>c :OSCYank<CR>

" vim-easymotion - core functionality mapped to <Leader><Leader> + command. Extras:
"   s plus two chars to jump into any split
nmap s <Plug>(easymotion-overwin-f2)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
"   Case-insensitive on lower case search
let g:EasyMotion_smartcase = 1



" Flags and Simple Options (vim and neovim)
set viminfo=           " don't use viminfo with encryption. backup and swap are ok
set tabstop=4          " Width of TAB is set to 4 spaces. Actual tap: {C-V Tab}
set shiftwidth=4       " Indents have width of 4
set softtabstop=4      " Number of columns for a TAB
set expandtab          " expands TABs into spaces
set ignorecase         " ignore case for slash searches...
set smartcase          " ...but switch to case sensitive if search string has upper case chars
                       " to search case sensitively: /searchstring\C  (\c makes it insensitive)
set title
set incsearch
set splitbelow         " default split below
set splitright         " default split right
set colorcolumn=100    " show vertical bar at 100th column
set list               " show listchars (see next line)
set listchars=tab:»·,trail:~  "show hard-tabs as »·, trailing spaces as ~
match ErrorMsg /\t/    " highlight hard tabs like	this
set fileformats=unix,dos " Prefer unix endings (don't create DOS CRLF on new files in Windows
set modeline           " Allows `# vim: filetype: apache` type modelines
set nohlsearch         " Disable highlighting search terms
set timeoutlen=500     " Timeout for mapped sequence (e.g. <leader>) (default=1000ms)
syntax on


" Configure cursor
if exists('$TMUX')
    let &t_SI = "\<esc>[5 q" " I beam cursor for insert mode
    let &t_EI = "\<esc>[2 q" " block cursor for normal mode
    let &t_SR = "\<esc>[3 q" " underline cursor for replace mode
else
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
endif

" Colors and Theme
set background=dark
let g:airline_theme='dark_minimal'
"let g:airline#extensions#tabline#enabled = 1  " shows list of tabs/buffers on the top line.
let g:gruvbox_italic=1
let g:gruvbox_bold=1
let g:gruvbox_contrast_dark="hard"
colorscheme gruvbox
if exists('+termguicolors')
    set termguicolors  " Enable 24-bit true color
endif


" vim-markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_new_list_item_indent = 2
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_emphasis_multiline = 0


" Allow saving of files as su when forgot to invoke vim with su
cmap w!! w !sudo tee > /dev/null %


" Line Numbers (rnu=relativenumber, nu=number) (nu! toggles only nu)
set nu rnu         " turns on hybrid number
augroup numbertoggle   " standard number on entering insert mode, hybrid on leave
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set nu rnu
    autocmd BufLeave,FocusLost,InsertEnter * set nu nornu
augroup END

" Override word-boundary options for ALL files (rules are set by nvim/runtime/syntax/*).
" Note: individual overrides should be in ~/.config/nvim/after/syntax/.
" Rule: _ and - are *always* word boundaries
augroup wordboundary_override
    autocmd!
    autocmd BufWinEnter,Winenter * set iskeyword-=_,-
augroup END

" Set terminal title to path + file
let &titlestring = $USER . "@" . hostname() . " " . expand("%:p")
if &term == "screen"
  set t_ts=^[k
  set t_fs=^[\
endif
if &term == "screen" || &term == "xterm"
  set title
endif


" Silence all bells
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=


" vim-airline symbols: define unicode symbols for portability
if !exists('g:airline_symbols')
    let g:airline_symbols = {}  " create dict if it doesn't exist
endif
let g:airline_left_sep = '►'
let g:airline_right_sep = '◄'
let g:airline_symbols.crypt = '#'
let g:airline_symbols.linenr = ' '
let g:airline_symbols.colnr = ' ㏇'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = 'Ѱ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.spell = 'Ƨ'
let g:airline_symbols.notexists = 'Ɇ'
let g:airline_symbols.whitespace = 'Ξ'


" Editor-specific Configuration (nvim first)
if has('nvim')
    " Set history (backup, swap, and undo) directory
    let hist_dir = $HOME . "/.local/share/nvim/hist_nvim"

    " Open new terminal in splits
    cabbrev term <C-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'split +term' : 'term')<CR>
    cabbrev vterm <C-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'vsplit +term' : 'vterm')<CR>

    " Use Alt + ; to go to normal mode, Alt + Shift + ; to go to command mode
    tnoremap <A-;> <C-\><C-n>
    tnoremap <A-:> <C-\><C-n>:
    " for terminal (same thing)
    tnoremap <C-j> <C-\><C-n><C-W>j
    tnoremap <C-k> <C-\><C-n><C-W>k
    tnoremap <C-l> <C-\><C-n><C-W>l
    tnoremap <C-h> <C-\><C-n><C-W>h
    tnoremap <C-w> <C-\><C-n><C-w>

    " start terminal in insert mode, close its split after exit
    augroup term_cmds
        autocmd!
        " Bypass normal mode when changing focus to terminal buffer
        autocmd BufWinEnter,Winenter term://* startinsert
        autocmd TermOpen term://* startinsert
        " Disable numbers when entering terminal mode, show hybrid when in normal mode
        autocmd TermEnter term://* setlocal nonu nornu
        autocmd TermLeave term://* setlocal nu rnu
        autocmd TermClose term://* close
        "autocmd TermClose * let b:term_job_finished = 1 | call feedkeys("\<C-\>\<C-n>")
    augroup END

    " Fixes 24-bit true color if TERM is not xterm, but doesn't harm anything if not needed
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" vim section
else
    " Set history (backup, swap, and undo) directory
    let hist_dir = $HOME . "/.local/share/nvim/hist_vim"

    set term=tmux-256color

    " Enable true color (24 bit)
    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"  " Fixes true color in tmux if TERM is not
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"  " xterm, but won't harm anything if not needed.
        set termguicolors
    endif

    " for terminal (same thing)
    tnoremap <C-j> <C-W>j
    tnoremap <C-k> <C-W>k
    tnoremap <C-l> <C-W>l
    tnoremap <C-h> <C-W>h

    " Use Alt + ; to go to normal mode
    tnoremap <A-;> <C-w><S-n>
    " Use Alt + Shift + ; to go to command mode
    tnoremap <A-:> <C-w><S-n>:
endif


" Common history directory section
let swap_dir = hist_dir . "/swap"
let backup_dir = hist_dir . "/backup"
let undo_dir = hist_dir . "/undo"
if !isdirectory(swap_dir)
    call mkdir(swap_dir, "p", 0700)
endif
if !isdirectory(backup_dir)
    call mkdir(backup_dir, "p", 0700)
endif
if !isdirectory(undo_dir)
    call mkdir(undo_dir, "p", 0700)
endif
" Using `let &` allows use of variables; `set` is limited to setting to a literal value
" // means use full path as filename, to avoid collisions between same filenames in difft paths
let &directory=swap_dir . "//"
let &backupdir=backup_dir . "//"
let &undodir=undo_dir . "//"
set backup
set undofile
