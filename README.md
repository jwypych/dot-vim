This repository is intended to be used as the XDG-specified configuration
directory for neovim, namely `~/.config/nvim`. To use it, make sure that
this directory doesn't exist and then clone this repo into it:
```
  $ git clone git@gitlab.com:jwypych/dot-vim.git ~/.config/nvim
```
# Supported Versions

  - neovim: 0.5+
  - vim must be 8+

# Making vim work

Create a symlink `~/.vimrc` pointing to `~/.config/nvim/init.vim`:
```
  $ ln -s ~/.config/nvim/init.vim ~/.vimrc
```
Make sure `~/.vim` does not exist, since it remains in vim's runtimepath and
if there are configs there, they may cause conflicts.

Strictly speaking. `init.vim` is a neovim config file, but it is written to be used
by both neovim and vim8, with conditional sections (on `has('nvim')`) as
required.

# Directory Contents
  - `after` - standard vim directory, configuration to apply/override after everything.
    - Note: strictly speaking, there should be a directory `~/.local/share/nvim`
            that overrides plugins (and other things defined in the `/share/nvim`
            directories. But in the interests of keeping my entire configuration
            in a single git repo, I use `~/.config/nvim/after` for *all* my custom
            overrides / late-apply configs.
    - Hint: run `:h runtimepath` to see order in which directories are parsed.
            This helps with figuring out what needs to be overriden where, but
            the general rule is that `~/.config/nvim/after` is *dead last*.
  - `old_config` - contains old vimrc and vimrc.simple files that can be used with
                 old vim setups.
  - `pack` - this *can* be used to load plugins using vim's or neovim's native
           plugin handling logic. Instead of built-in package manageemnt, I use
           `vim-plug,` which is more flexible and easier to set up on new machines,
           since it can self-install using a simple `vimscript` stanza in the config file.
    - Indeed, my `init.vim` has `vim-plug` install itself and all my declared plugins
      if `vim-plug` is missing.
    - Note: if `vim-plug` is installed, I do *not* have it try to `PlugInstall` on startup.

