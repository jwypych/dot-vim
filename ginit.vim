" Settings for GUI nvims (like nvim-qt)

" Enable Mouse
set mouse=a

" Set Editor Font
if exists(':GuiFont')
    " Use GuiFont! to ignore font errors
    GuiFont! Hack:h9
endif

" Disable GUI Tabline
if exists(':GuiTabline')
    GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 0
endif

" Enable GUI ScrollBar
if exists(':GuiScrollBar')
    GuiScrollBar 1
endif

" Right Click Context Menu (Copy-Cut-Paste)
nnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>
inoremap <silent><RightMouse> <Esc>:call GuiShowContextMenu()<CR>
vnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>gv

" Change font with Ctrl+mouse_scroll
function ScaleFont(amount)
  " Call function with bang ("!") to suppress errors
  call GuiFont(substitute(&guifont, '\:h\(\d\+\)', '\=":h" . (submatch(1) + a:amount)', ''), "!")
endfunction
noremap <C-ScrollWheelUp> :call ScaleFont(1)<CR>
noremap <C-ScrollWheelDown> :call ScaleFont(-1)<CR>
inoremap <C-ScrollWheelUp> <Esc>:call ScaleFont(1)<CR>a
inoremap <C-ScrollWheelDown> <Esc>:call ScaleFont(-1)<CR>a
